#include<iostream>
#include <conio.h>

using namespace std;

int main() {
    cout << "Kalkulator" << endl;
    do {
        cout << "Wybierz odpowiednie dzialanie" << endl << endl
             << "1. Dodawanie" << endl
             << "2. Odejmowanie" << endl
             << "3. Mnozenie" << endl
             << "4. Dzielenie" << endl
             << "0. Wyjscie" << endl << endl;
        cout << "Wpisz swoj wybor: " << endl;
        int select;
        cin >> select;
        if (select == 0) {
            return 0;
        }
        cout << "Podaj 1 liczbe: " << endl;
        double num1;
        cin >> num1;
        cout << "Podaj 2 liczbe: " << endl;
        double num2;
        cin >> num2;
        switch (select) {
            case 1 :
                cout << "Wynik = " << num1 + num2 << endl;
                break;
            case 2:
                cout << "Wynik = " << num1 - num2 << endl;
                break;
            case 3:
                cout << "Wynik = " << num1 * num2 << endl;
                break;
            case 4:
                cout << "Wynik = " << num1 / num2 << endl;
                break;
            default:
                cout << "Bledne dane." << endl;
                break;
        }
        cout << endl << "Wpisz 'e' aby uruchomic kalkulator od nowa." << endl << endl;
    } while (getch() == 'e');
}